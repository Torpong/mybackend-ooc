package io.muic.ooc.webapp;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

/**
 * Created by bossy on 7/7/2560.
 */
public class MyConstants {
    public static final AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
            .withCredentials(new ProfileCredentialsProvider())
            .withRegion(Regions.AP_SOUTHEAST_1)
            .build();
    public static final String bucketName = "seniorproject-muic";
}
