package io.muic.ooc.webapp.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by bossy on 5/4/2560.
 */
@Entity
public class Post {

    @Id
    @GeneratedValue
    private long id;

    private String jobTitle;

    private String jobDescription;

    private String jobLocation;

    private String companyEmail;

    private String approvedStudent;

    private String requestStudent;

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public String getJobTitle() { return jobTitle; }

    public void setJobTitle(String jobTitle) { this.jobTitle = jobTitle; }

    public String getJobDescription() { return jobDescription; }

    public void setJobDescription(String jobDescription) { this.jobDescription = jobDescription; }

    public String getJobLocation() { return jobLocation; }

    public void setJobLocation(String jobLocation) { this.jobLocation = jobLocation; }

    public String getCompanyEmail() { return companyEmail; }

    public void setCompanyEmail(String companyEmail) { this.companyEmail = companyEmail; }

    public String getApprovedStudent() { return approvedStudent; }

    public void setApprovedStudent(String approvedStudent) { this.approvedStudent = approvedStudent; }

    public String getRequestStudent() { return requestStudent; }

    public void setRequestStudent(String requestStudent) { this.requestStudent = requestStudent; }

}
