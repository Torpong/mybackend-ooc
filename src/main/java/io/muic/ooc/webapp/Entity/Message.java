package io.muic.ooc.webapp.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.awt.event.ComponentAdapter;

/**
 * Created by bossy on 5/4/2560.
 */
@Entity
public class Message {

    @Id
    @GeneratedValue
    private long id;

    private String sender;

    private String receiver;

    private String title;

    private String message;

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public String getSender() { return sender; }

    public void setSender(String sender) { this.sender = sender; }

    public String getReceiver() { return receiver; }

    public void setReceiver(String receiver) { this.receiver = receiver; }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getMessage() { return message; }

    public void setMessage(String message) { this.message = message; }
}
