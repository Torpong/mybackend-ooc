package io.muic.ooc.webapp.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by bossy on 12/3/2560.
 */
@Entity
public class Student {

    @Id
    @GeneratedValue
    private long id;

    private String firstName;

    private String lastName;

    private String email;

    private String userType;

    private ArrayList<String> favCompany = new ArrayList<String>();

    private ArrayList<String> skills  = new ArrayList<String>();

    private ArrayList<String> workExperience  = new ArrayList<String >();

    private ArrayList<String> education = new ArrayList<String>();

    @OneToOne
    @JoinColumn(name="user_entity_id")
    private User user;

    //    @JsonIgnore
    @OneToMany
    @JoinColumn(name="message_entity_id")
    private List<Message> message;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserType() { return userType; }

    public void setUserType(String userType) { this.userType = userType; }

    public User getUser() { return user; }

    public void setUser(User user) { this.user = user; }

    public ArrayList<String> getFavCompany() { return favCompany; }

    public void setFavCompany(ArrayList<String> favCompany) { this.favCompany = favCompany; }

    public ArrayList<String> getSkills() { return skills; }

    public void setSkills(ArrayList<String> skills) { this.skills = skills; }

    public ArrayList<String> getWorkExperience() { return workExperience; }

    public void setWorkExperience(ArrayList<String> workExpeience) { this.workExperience = workExpeience; }

    public ArrayList<String> getEducation() { return education; }

    public void setEducation(ArrayList<String> education) { this.education = education; }

    public List<Message> getMessage() { return message; }

    public void setMessage(List<Message> message) { this.message = message; }

}