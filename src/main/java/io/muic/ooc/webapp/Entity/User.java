package io.muic.ooc.webapp.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by bossy on 23/3/2560.
 */
@Entity
public class User {
    @Id
    @GeneratedValue
    private long id;

    @Column(name="username")
    private String username;

    @Column(name="password")
    private String password;

    private String role;

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public String getRole() { return role; }

    public void setRole(String role) { this.role = role; }
}
