package io.muic.ooc.webapp.Entity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by bossy on 12/3/2560.
 */
@Entity
public class Company {

    @Id
    @GeneratedValue
    private long id;

    private String companyName ;

    private String companyDescription;

    private String email;

    private long phoneNumber;

    private String username;

    private String password;

    private String userType;

    @OneToOne
    @JoinColumn(name="user_entity_id")
    private User user;

    @OneToMany
    @JoinColumn(name="post_entity_id")
    private List<Post> post;

    @OneToMany
    @JoinColumn(name="message_entity_id")
    private List<Message> message;

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public String getCompanyName() { return companyName; }

    public void setCompanyName(String companyName) { this.companyName = companyName; }

    public String getCompanyDescription() { return companyDescription; }

    public void setCompanyDescription(String companyDescription) { this.companyDescription = companyDescription; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public long getPhoneNumber() { return phoneNumber; }

    public void setPhoneNumber(long phoneNumber) { this.phoneNumber = phoneNumber; }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public String getUserType() { return userType; }

    public void setUserType(String userType) { this.userType = userType; }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Post> getPost() { return post; }

    public void setPost(List<Post> post) { this.post = post; }

    public List<Message> getMessage() { return message; }

    public void setMessage(List<Message> message) { this.message = message;}
}