package io.muic.ooc.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class WebApplication {

    public static void main(String[] args) {

        SpringApplication.run(WebApplication.class, args);
    }

   @Bean
   public WebMvcConfigurer corsConfigurer() {
       return new WebMvcConfigurerAdapter() {
           @Override
           public void addCorsMappings(CorsRegistry registry) {
               registry.addMapping("/students").allowedOrigins("http://localhost:8080");
               registry.addMapping("/company").allowedOrigins("http://localhost:8080");
               registry.addMapping("/login").allowedOrigins("http://localhost:8080");
               registry.addMapping("/register").allowedOrigins("http://localhost:8080");
               registry.addMapping("/post").allowedOrigins("http://localhost:8080");
               registry.addMapping("/upload").allowedOrigins("http://localhost:8080");
           }
       };
   }
}