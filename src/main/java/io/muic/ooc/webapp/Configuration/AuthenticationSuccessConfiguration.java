package io.muic.ooc.webapp.Configuration;

import com.google.gson.JsonObject;
import io.muic.ooc.webapp.Entity.User;
import io.muic.ooc.webapp.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by bossy on 4/4/2560.
 */
@Component
public class AuthenticationSuccessConfiguration extends SimpleUrlAuthenticationSuccessHandler{

    @Autowired
    UserRepository userRepository;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException{
        User tempLogIn = userRepository.findByUsername(authentication.getName());
        response.setStatus(200);
        JsonObject temp = new JsonObject();
        temp.addProperty("status",true);
        temp.addProperty("role",tempLogIn.getRole());
        response.setContentType("application/json");
        response.getWriter().write(temp.toString());
        clearAuthenticationAttributes(request);
    }
}
