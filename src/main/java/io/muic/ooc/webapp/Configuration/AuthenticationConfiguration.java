package io.muic.ooc.webapp.Configuration;

import io.muic.ooc.webapp.Entity.User;
import io.muic.ooc.webapp.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;

import javax.naming.AuthenticationException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bossy on 4/4/2560.
 */
@Component
public class AuthenticationConfiguration implements AuthenticationProvider {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws org.springframework.security.core.AuthenticationException {
//        TODO: Authenticate with DB that user exist and have valid credentials
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();
        if(Login(username,password)){
            return new UsernamePasswordAuthenticationToken(username, null, new ArrayList<>());
        }
        return null;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }

    boolean Login(String  username, String password){
        User temp = userRepository.findByUsername(username);
        System.out.println(temp);
        System.out.println("WE ARE IN LOGIN PROCESS");
        System.out.println(username);
        if(temp == null){
            return false;
        }
        if(BCrypt.checkpw(password, temp.getPassword())) {
            return true;
        }else {
            return false;
        }
//        Student temp = new Student("Torpong","Juntree","Torpong.boss@gmail.com",5680496,
//                "LycanNeo","dkhdoh");
    }
}
