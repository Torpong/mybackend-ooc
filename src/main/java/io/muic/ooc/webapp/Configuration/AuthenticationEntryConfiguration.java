package io.muic.ooc.webapp.Configuration;

import com.google.gson.JsonObject;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by bossy on 5/4/2560.
 */
@Component
public class AuthenticationEntryConfiguration implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("status", false);

        response.getWriter().write(jsonObject.toString());
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
}
