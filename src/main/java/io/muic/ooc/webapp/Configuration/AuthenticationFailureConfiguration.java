package io.muic.ooc.webapp.Configuration;

import com.google.gson.JsonObject;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.naming.AuthenticationException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by bossy on 5/4/2560.
 */
@Component
public class AuthenticationFailureConfiguration extends SimpleUrlAuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, org.springframework.security.core.AuthenticationException exception) throws IOException, ServletException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

        JsonObject temp = new JsonObject();
        temp.addProperty("status",false);

        response.setContentType("application/json");
        response.getWriter().write(temp.toString());
    }
}
