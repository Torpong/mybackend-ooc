package io.muic.ooc.webapp.Controller;

/**
 * Created by bossy on 12/3/2560.
 */
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.muic.ooc.webapp.Entity.Company;
import io.muic.ooc.webapp.Entity.Student;
import io.muic.ooc.webapp.Entity.User;
import io.muic.ooc.webapp.Repository.StudentRepository;
import io.muic.ooc.webapp.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;


// Principal principal
// principal.getname
@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/students")
public class StudentController {
    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Student>> allStudentUsers() {
        System.out.println("HELLO GET");
        List<Student> users = studentRepository.findAll();
        if(users.isEmpty()){
            return new ResponseEntity<List<Student>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Student>>(users, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{email:.+}")
    public ResponseEntity<Student> oneStudentUsers(@PathVariable String email) {
        Student users = studentRepository.findByEmail(email);
        if(users == null){
            return new ResponseEntity<Student>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Student>(users, HttpStatus.OK);
    }

    @RequestMapping(method =  RequestMethod.POST, value = "/edit")
    public ResponseEntity<Void> EditProfile(@RequestBody Map<String, Object> payload, Principal principal){
        System.out.println("WELCOME");
        String editor = payload.get("email").toString();
        if(editor.equals(principal.getName())){
            System.out.println("EDITING");
            Student edit = studentRepository.findByEmail(principal.getName());
            edit.setFirstName(payload.get("firstName").toString());
            edit.setLastName(payload.get("lastName").toString());
            edit.setSkills((ArrayList<String>)payload.get("skillSet"));
            edit.setWorkExperience((ArrayList<String>)payload.get("workingExperience"));
            edit.setEducation((ArrayList<String>)payload.get("education"));


            studentRepository.save(edit);
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}