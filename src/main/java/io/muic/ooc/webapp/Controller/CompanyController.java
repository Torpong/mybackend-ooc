package io.muic.ooc.webapp.Controller;

/**
 * Created by bossy on 12/3/2560.
 */
import java.security.Principal;
import java.util.List;
import java.util.Map;

import io.muic.ooc.webapp.Entity.Company;
import io.muic.ooc.webapp.Repository.CompanyRepository;
import io.muic.ooc.webapp.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/company")
public class CompanyController {
    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Company>> allCompanyUsers() {
        List<Company> users = companyRepository.findAll();
        if(users.isEmpty()){
            return new ResponseEntity<List<Company>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Company>>(users, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{email:.+}")
    public ResponseEntity<Company> OneCompanyUsers(@PathVariable String email) {
        Company users = companyRepository.findByEmail(email);
        if(users == null){
            return new ResponseEntity<Company>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Company>(users, HttpStatus.OK);
    }

    @RequestMapping(method =  RequestMethod.POST, value = "/edit")
    public ResponseEntity<Void> EditProfile(@RequestBody Map<String, Object> payload, Principal principal){
        String editor = payload.get("email").toString();
        if(editor.equals(principal.getName())){
            Company edit = companyRepository.findByEmail(principal.getName());
            edit.setPhoneNumber(Long.parseLong(payload.get("phoneNumber").toString()));
            edit.setCompanyDescription(payload.get("companyDesciption").toString());
            edit.setCompanyDescription(payload.get("companyName").toString());
        }
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}