package io.muic.ooc.webapp.Controller;

/**
 * Created by bossy on 12/3/2560.
 */
import java.io.File;
import java.io.FileOutputStream;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import io.muic.ooc.webapp.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import static io.muic.ooc.webapp.MyConstants.bucketName;
import static io.muic.ooc.webapp.MyConstants.s3Client;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/upload")
public class UploadController {

    @Autowired
    private UserRepository userRepository;



    @RequestMapping(method = RequestMethod.GET, value = "/view")
    public void view() {
        System.out.println("\n ===== viewObjectsInBucket() =====");

        try {
            System.out.println("Listing all objects in " + bucketName);
            final ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucketName).withMaxKeys(2);
            ListObjectsV2Result result;
            do {
                result = s3Client.listObjectsV2(req);

                for (S3ObjectSummary objectSummary :
                        result.getObjectSummaries()) {
                    System.out.println(" - " + objectSummary.getKey() + "  " +
                            "(size = " + objectSummary.getSize() +
                            ")");
                }
                System.out.println("Next Continuation Token : " + result.getNextContinuationToken());
                req.setContinuationToken(result.getNextContinuationToken());
            } while (result.isTruncated() == true);

        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, " +
                    "which means your request made it " +
                    "to Amazon S3, but was rejected with an error response " +
                    "for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());

        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, " +
                    "which means the client encountered " +
                    "an internal error while trying to communicate" +
                    " with S3, " +
                    "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/putFile")
    public String upload(@RequestParam("name") String name,
                         @RequestParam("file") MultipartFile file){
        if (!file.isEmpty()) {
            try {
                TransferManager tm = new TransferManager(s3Client);
                File convFile = new File(file.getOriginalFilename());
                convFile.createNewFile();
                FileOutputStream fos = new FileOutputStream(convFile);
                fos.write(file.getBytes());
                fos.close();
                Upload upload = tm.upload(bucketName, convFile.getName(), convFile);
                upload.waitForCompletion();
                convFile.delete();

                return "You successfully uploaded " + name + "!";
            } catch (Exception e) {
                return "You failed to upload " + name + " => " + e.getMessage();
            }
        } else {
            return "You failed to upload " + name + " because the file was empty.";
        }
    }


}