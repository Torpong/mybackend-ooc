package io.muic.ooc.webapp.Controller;
import io.muic.ooc.webapp.Entity.*;
import io.muic.ooc.webapp.Repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.*;

import java.util.*;
/**
 * Created by bossy on 5/4/2560.
 */
@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private CompanyRepository companyRepository;

//    @Autowired
//    private MessageRepository messageRepository;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> register(@RequestBody Map<String, Object> payload) throws Exception {
        System.out.println("THIS IS REGISTER");
        System.out.println(payload);

        String role = payload.get("userType").toString();
        String username = payload.get("email").toString();
        String password = payload.get("password").toString();
        String pw_hash = BCrypt.hashpw(password, BCrypt.gensalt());
        User findUser = userRepository.findByUsername(username);

        if(findUser == null) {
            System.out.println("OK TO CREATE");
            System.out.println(role);
            User usertemp = new User();
            usertemp.setUsername(username);
            usertemp.setPassword(pw_hash);
            usertemp.setRole(role);
            userRepository.save(usertemp);
            if(role.equals("company")) {
                System.out.println("THIS IS COMPANY");
                //Add student content to database
                Company temp = new Company();
                temp.setUser(usertemp);

                temp.setCompanyName(payload.get("companyName").toString());
                temp.setCompanyDescription(payload.get("companyDescription").toString());
                temp.setEmail(username);
                temp.setPhoneNumber(Long.parseLong(payload.get("phoneNumber").toString()));
                temp.setUserType(role);

                companyRepository.save(temp);
                return new ResponseEntity<Void>(HttpStatus.CREATED);

            }else{
                System.out.println("IN STUDENT");
                //Add student content to database
                Student temp = new Student();
                temp.setUser(usertemp);
                temp.setFirstName(payload.get("firstName").toString());
                temp.setLastName(payload.get("lastName").toString());
                temp.setEmail(username);
                temp.setUserType(role);

                studentRepository.save(temp);
                return new ResponseEntity<Void>(HttpStatus.CREATED);
            }
        }
        return new ResponseEntity<Void>(HttpStatus.NOT_ACCEPTABLE);
    }
}
