package io.muic.ooc.webapp.Controller;

import io.muic.ooc.webapp.Entity.Company;
import io.muic.ooc.webapp.Entity.Message;
import io.muic.ooc.webapp.Entity.Student;
import io.muic.ooc.webapp.Entity.User;
import io.muic.ooc.webapp.Repository.CompanyRepository;
import io.muic.ooc.webapp.Repository.MessageRepository;
import io.muic.ooc.webapp.Repository.StudentRepository;
import io.muic.ooc.webapp.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by bossy on 5/4/2560.
 */
@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/message")
public class MessageController {
    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Message>> allMessage() {
        List<Message> users = messageRepository.findAll();
        if(users.isEmpty()){
            return new ResponseEntity<List<Message>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Message>>(users, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> oneMessage(@RequestBody Map<String, Object> payload ) throws Exception {
        String saver = payload.get("receiver").toString();
        User temp = userRepository.findByUsername(saver);
        if(temp != null) {
            Message usertemp = new Message();
            usertemp.setSender(payload.get("sender").toString());
            usertemp.setReceiver(saver);
            usertemp.setMessage(payload.get("message").toString());
            usertemp.setTitle(payload.get("title").toString());
            messageRepository.save(usertemp);

            if (temp.getRole().equals("students")) {
                Student receiver = studentRepository.findByEmail(saver);
                List<Message> addIn = receiver.getMessage();
                addIn.add(usertemp);
                receiver.setMessage(addIn);
                studentRepository.save(receiver);
                return new ResponseEntity<Void>(HttpStatus.CREATED);
            } else {
                Company receiver = companyRepository.findByEmail(saver);
                List<Message> addIn = receiver.getMessage();
                addIn.add(usertemp);
                receiver.setMessage(addIn);
                companyRepository.save(receiver);
                return new ResponseEntity<Void>(HttpStatus.CREATED);

            }
        }
        return new ResponseEntity<Void>(HttpStatus.NOT_ACCEPTABLE);
    }
}
