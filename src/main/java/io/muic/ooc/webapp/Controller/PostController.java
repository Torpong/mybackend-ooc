package io.muic.ooc.webapp.Controller;

import io.muic.ooc.webapp.Entity.*;
import io.muic.ooc.webapp.Repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by bossy on 5/4/2560.
 */

// Principal principal
// principal.getname
@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/post")
public class PostController {
    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Post>> allPosts() {
        List<Post> users = postRepository.findAll();
        if(users.isEmpty()){
            return new ResponseEntity<List<Post>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Post>>(users, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> createPost(@RequestBody Map<String, Object> payload, @RequestHeader MultiValueMap<String,String> head) throws Exception {
        String saver = payload.get("companyEmail").toString();
        User temp = userRepository.findByUsername(saver);
        if(temp != null) {

            Post usertemp = new Post();
            usertemp.setJobTitle(payload.get("jobTitle").toString());
            usertemp.setJobDescription(payload.get("jobDescription").toString());
            usertemp.setJobLocation(payload.get("jobLocation").toString());
            usertemp.setCompanyEmail(saver);
            usertemp.setApprovedStudent(payload.get("approvedStudent").toString());
            usertemp.setRequestStudent(payload.get("requestStudent").toString());
            postRepository.save(usertemp);

            Company receiver = companyRepository.findByEmail(saver);
            List<Post> addIn = receiver.getPost();
            addIn.add(usertemp);
            receiver.setPost(addIn);
            companyRepository.save(receiver);
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        }
        return new ResponseEntity<Void>(HttpStatus.NOT_ACCEPTABLE);
    }
}
