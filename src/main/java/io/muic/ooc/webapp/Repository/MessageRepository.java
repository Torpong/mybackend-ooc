package io.muic.ooc.webapp.Repository;

import io.muic.ooc.webapp.Entity.Message;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by bossy on 5/4/2560.
 */
@Repository
public interface MessageRepository  extends CrudRepository<Message, Long> {

    Message findById(long id);

    List<Message> findAll();
}
