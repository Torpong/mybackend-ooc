package io.muic.ooc.webapp.Repository;

import io.muic.ooc.webapp.Entity.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by bossy on 5/4/2560.
 */
@Repository
public interface PostRepository extends CrudRepository<Post, Long> {

    Post findById(long id);

    List<Post> findAll();

}
