package io.muic.ooc.webapp.Repository;

import io.muic.ooc.webapp.Entity.Student;
//import io.muic.ooc.webapp.Entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by bossy on 12/3/2560.
 */
@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {

//    Student findByUsername(String username);

    Student findByEmail(String email);

    Student findById(long id);

    List<Student> findAll();
}
