package io.muic.ooc.webapp.Repository;

import io.muic.ooc.webapp.Entity.Company;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by bossy on 12/3/2560.
 */
@Repository
public interface CompanyRepository extends CrudRepository<Company, Long> {

    Company findByUsername(String username);

    Company findByEmail(String email);

    Company findById(long id);

    List<Company> findAll();
}
