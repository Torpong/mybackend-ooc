package io.muic.ooc.webapp.Repository;

import io.muic.ooc.webapp.Entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by bossy on 23/3/2560.
 */
public interface UserRepository extends CrudRepository<User, Long> {

    User findByUsername(String username);

    User findById(long id);

    List<User> findAll();
}
